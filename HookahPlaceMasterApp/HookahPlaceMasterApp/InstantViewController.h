//
//  InstantViewController.h
//  HookahPlaceMasterApp
//
//  Created by Indieg0 on 12.05.16.
//  Copyright © 2016 Kirill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstantViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
- (IBAction)doneButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *enterLabel;

@end
