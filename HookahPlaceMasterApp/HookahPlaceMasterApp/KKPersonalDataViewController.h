//
//  KKPersonalDataViewController.h
//  HookahPlaceMasterApp
//
//  Created by Indieg0 on 12.04.16.
//  Copyright © 2016 Kirill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KKPersonalDataViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *identifierLabel;
@property (weak, nonatomic) IBOutlet UILabel *bonusQuantityLabel;
@property (weak, nonatomic) IBOutlet UITextField *addBonusTextField;
@property (weak, nonatomic) IBOutlet UITextField *subtractBonusField;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (strong,nonatomic) NSString *identity;


- (IBAction)doneButtonAction:(UIButton *)sender;

@end
