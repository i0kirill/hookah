//
//  TableViewController.h
//  HookahPlaceMasterApp
//
//  Created by Indieg0 on 17.04.16.
//  Copyright © 2016 Kirill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *testLabel;
@property (strong, nonatomic) NSString *identity;
@end
