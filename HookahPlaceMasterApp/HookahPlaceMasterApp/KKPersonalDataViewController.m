//
//  KKPersonalDataViewController.m
//  HookahPlaceMasterApp
//
//  Created by Indieg0 on 12.04.16.
//  Copyright © 2016 Kirill. All rights reserved.
//

#import "KKPersonalDataViewController.h"
#import "Parse.h"
#import "TableViewController.h"

@interface KKPersonalDataViewController ()

@end

@implementation KKPersonalDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(handleSingleTap:)];
    
    [self.view addGestureRecognizer:tap];
    
    self.identifierLabel.text = self.identity;
    
    PFQuery *query = [PFQuery queryWithClassName:@"HookahUser"];
    [query getObjectInBackgroundWithId:self.identity  block:^(PFObject *hookahUser, NSError *error) {
        if (!error) {
            self.nameLabel.text = [hookahUser objectForKey:@"name"];
            self.lastNameLabel.text = [hookahUser objectForKey:@"lastName"];
            self.phoneLabel.text = [hookahUser objectForKey:@"phoneNumber"];
            self.bonusQuantityLabel.text = [[hookahUser objectForKey:@"bonusQuantity"]stringValue];
            NSLog(@"Successfully retrieved");
        } else {
            NSLog(@"Error!");
        }
        
    }];
    
    
    // Do any additional setup after loading the view.
}

- (void) viewDidAppear:(BOOL)animated {
 
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.addBonusTextField]) {
        [self.subtractBonusField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender {
    [self.view endEditing:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"pushToLog"]) {
     
        
    }
}

- (IBAction)doneButtonAction:(UIButton *)sender {
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"HookahUser"];
    [query getObjectInBackgroundWithId:self.identity  block:^(PFObject *hookahUser, NSError *error) {
        if (!error) {
          
           int currentValue = [[hookahUser objectForKey:@"bonusQuantity"] intValue];
            
            currentValue = currentValue - [self.subtractBonusField.text intValue];
            
           
            [hookahUser setObject:[NSNumber numberWithInt:[self.addBonusTextField.text intValue]] forKey:@"addBonusQuantity"];
            [hookahUser setObject:[NSNumber numberWithInt:currentValue] forKey:@"bonusQuantity"];
            [hookahUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
                if (succeeded){
                    NSLog(@"Object Uploaded!");
                }
                else{
                    NSString *errorString = [[error userInfo] objectForKey:@"error"];
                    NSLog(@"Error: %@", errorString);
                }
                
            }];
            
        int delta = [self.addBonusTextField.text intValue] - [self.subtractBonusField.text intValue];
        PFObject *log = [PFObject objectWithClassName:@"Log"];
        [log setObject:[hookahUser objectId] forKey:@"userId"];
        [log setObject:[hookahUser updatedAt] forKey:@"userUpdate"];
        [log setObject:[NSNumber numberWithInt:delta] forKey:@"delta"];
        [log setObject:[hookahUser objectForKey:@"name"] forKey:@"name"];
        [log setObject:[hookahUser objectForKey:@"lastName"] forKey:@"lastName"];
        [log setObject:[hookahUser objectForKey:@"phoneNumber"] forKey:@"phoneNumber"];
        [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (succeeded){
                NSLog(@"Object Uploaded!");
                
                TableViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"TableViewController"];
                vc.identity = self.identity;
                [self showViewController:vc sender:self];
                
                
          
            }
            else{
                NSString *errorString = [[error userInfo] objectForKey:@"error"];
                NSLog(@"Error: %@", errorString);
            }
        }];
            
            
        } else {
            NSLog(@"Error!");
        }
        
    }];
    
}
@end
