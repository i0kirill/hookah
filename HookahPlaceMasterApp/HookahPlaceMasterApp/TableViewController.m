//
//  TableViewController.m
//  HookahPlaceMasterApp
//
//  Created by Indieg0 on 17.04.16.
//  Copyright © 2016 Kirill. All rights reserved.
//

#import "TableViewController.h"
#import "Parse.h"
#import "User.h"

@interface TableViewController ()
@property (strong, nonatomic) NSMutableArray *objects;

@end

@implementation TableViewController
- (IBAction)refresh:(id)sender {
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector (addObjectsNotification:)
                                                 name:@"addObjects"
                                               object:nil];
    

    PFQuery *query = [PFQuery queryWithClassName:@"Log"];
    [query whereKey:@"userId" equalTo:self.identity];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
       
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"addObjects"
                                                            object:objects
                                                          userInfo:nil];
        
    }];
    
  
    
    
}

- (void) viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];

}

- (void) addObjectsNotification: (NSNotification*) notification {
    
    NSMutableArray *array = [NSMutableArray array];
    for (id object in notification.object) {
        
        
        
        User *user = [[User alloc] init];
                user.userID = [object objectForKey:@"userId"];
                user.delta = [object objectForKey:@"delta"];
                user.userUpdate = [object objectForKey:@"userUpdate"];
                [array addObject:user];
    //    NSLog(@"Delta %@", user.delta);
        
        // NSLog(@"Arr count %d", array.count);
        
           }
   // NSLog(@"Arr count %d", array.count);
    

    self.objects = array;
    
   // NSLog(@"Arr count %d", self.objects.count);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* identifier = @"Cell";
    

    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];

    
    User *user = [[User alloc] init];
    user = [self.objects objectAtIndex:indexPath.row];
    
    NSString *dateString = [NSDateFormatter localizedStringFromDate:user.userUpdate
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterShortStyle];
    
    cell.textLabel.text = [user.delta stringValue];
    cell.detailTextLabel.text = dateString;
    NSLog(@"user delta = %@", user.delta);
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
