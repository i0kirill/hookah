//
//  User.h
//  HookahPlaceMasterApp
//
//  Created by Indieg0 on 18.04.16.
//  Copyright © 2016 Kirill. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSNumber *delta;
@property (nonatomic, strong) NSDate *userUpdate;
@end
