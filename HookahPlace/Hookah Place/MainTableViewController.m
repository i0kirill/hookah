//
//  MainTableViewController.m
//  Hookah Place
//
//  Created by Denis on 20.04.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "MainTableViewController.h"

@interface MainTableViewController ()

@end

@implementation MainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ((indexPath.section == 1) && (indexPath.row == 0)) {
        NSURL *URL = [NSURL URLWithString:@"tel://89036661212"];
        [[UIApplication sharedApplication] openURL:URL];
    }
    else if ((indexPath.section == 1) && (indexPath.row == 1)) {
        
    }
    else if ((indexPath.section == 2) && (indexPath.row == 0)) {
        [self vk:nil];
    }
    else if ((indexPath.section == 2) && (indexPath.row == 1)) {
        [self inst:nil];
    }
}

- (IBAction)vk:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Переход в группу ВКонтакте" message:nil delegate:self cancelButtonTitle:@"Отмена" otherButtonTitles:@"Перейти", nil];
    alert.tag = 0;
    [alert show];
}
- (IBAction)inst:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Переход на страницу Facebook" message:nil delegate:self cancelButtonTitle:@"Отмена" otherButtonTitles:@"Перейти", nil];
    alert.tag = 1;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ((buttonIndex == 1) && (alertView.tag == 0)) {
        NSURL *URL = [NSURL URLWithString:@"http://vk.com/bonapart_msk"];
        [[UIApplication sharedApplication] openURL:URL];
    }
    if ((buttonIndex == 1) && (alertView.tag == 1)) {
        NSURL *URL = [NSURL URLWithString:@"http://www.facebook.com/bonaparte.pro"];
        [[UIApplication sharedApplication] openURL:URL];
    }
}

#pragma mark - Table view data source



/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
