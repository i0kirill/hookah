//
//  HookahTableViewController.m
//  Hookah Place
//
//  Created by Denis on 17.01.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "HookahTableViewController.h"

@interface HookahTableViewController ()

@end

@implementation HookahTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.tableView.backgroundColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"*Цена зависит\n от количества человек"
                                                        message:@"1-2 чел. - 1 КАЛЬЯН - 500₽/ЧАС\n3-4 чел. - 2 КАЛЬЯНА - 1000₽/ЧАС\n5 чел. - 3 КАЛЬЯНА - 1500₽/ЧАС\n6+ чел. - 4 КАЛЬЯНА - 2000₽/ЧАС\nДоп. кальян — 500₽"
                                                       delegate:nil
                                              cancelButtonTitle:@"Спасибо, знаем ;-)"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

@end
