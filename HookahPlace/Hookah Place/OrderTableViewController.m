//
//  OrderTableViewController.m
//  Hookah Place
//
//  Created by Denis on 17.01.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "OrderTableViewController.h"

@interface OrderTableViewController ()

@end

@implementation OrderTableViewController

/*
 - (void) sendToParse {
 PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
 testObject[/Users/denis/Desktop/Избранные проекты/AnyHookah/AnyHookah/My Images/MapCafe@2x.png@"name"] = self.nameLabel.text;
 testObject[@"tel"] = self.telLabel.text;
 testObject[@"card"] = self.cardNumberTextField.text;
 testObject[@"guestsCount"] = self.label.text;
 testObject[@"time"] = self.timeLabel.text;
 testObject[@"date"] = self.dateLabel.text;
 [testObject saveInBackground];
 }
 */

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
 //   self.tableView.backgroundColor = [UIColor blackColor];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
}

-(void)hideKeyBoard {
    [self.nameLabel resignFirstResponder];
    [self.telLabel resignFirstResponder];
    [self.timeLabel resignFirstResponder];
    [self.dateLabel resignFirstResponder];
    [self.cardNumberTextField resignFirstResponder];
    [self.shishasTextField resignFirstResponder];
}

- (IBAction)valueChanged:(UIStepper *)sender {
    double value = [sender value];
    
    [self.label setText:[NSString stringWithFormat:@"%d", (int)value]];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.telLabel resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toMakeOrder:(id)sender {
    if (!((![self.nameLabel.text  isEqual: @""]) && (![self.telLabel.text  isEqual: @""]) && (![self.cardNumberTextField.text  isEqual: @""]) && (![self.label.text  isEqual: @"0"]) && (![self.timeLabel.text  isEqual: @""]) && (![self.dateLabel.text  isEqual: @""])))
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Требуется заполнить все поля."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        
        /*
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Заявка на бронирование принята"
                                                        message:@"В ближайшее время с Вами свяжется наш менеджер для подтверждения брони."
                                                       delegate:nil
                                              cancelButtonTitle:@"Спасибо"
                                              otherButtonTitles:nil];
        [alert show];
        */
        
       // [self sendToParse];
        
        [self sendEmail];
        
        [self clearRows];
    }

}

- (void) clearRows {
    self.nameLabel.text = @"";
    self.telLabel.text = @"";
    self.cardNumberTextField.text = @"";
    self.label.text = @"0";
    self.timeLabel.text = @"";
    self.dateLabel.text = @"";
    self.shishasTextField.text = @"";
}

// Send e-mail

- (void) sendEmail {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        NSString *subjectString = [NSString stringWithFormat:@"Бронирование на %@ %@", self.timeLabel.text, self.dateLabel.text];
        [controller setSubject:subjectString];
        NSString *messagebodyString = [NSString stringWithFormat:@"Проверьте правильность введенных данных!\n\nФИО: %@\nТелефон: %@\nКарта №: %@\nКол-во человек: %@\nВремя: %@\nДата: %@\nКальяны: %@\n", self.nameLabel.text, self.telLabel.text, self.cardNumberTextField.text, self.label.text, self.timeLabel.text, self.dateLabel.text, self.shishasTextField.text];
        [controller setMessageBody:messagebodyString isHTML:NO];
        [controller setToRecipients:[NSArray arrayWithObjects:@"Lemon.haze.msk@gmail.com",nil]];
        if (controller) [self presentModalViewController:controller animated:YES];
    } else {
        [self sorryMessagingIsUnavailable];
    }
}


- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
        
        [self messageWasSent];
        
    }
    [self dismissModalViewControllerAnimated:YES];
}

- (void) messageWasSent {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Заявка на бронирование принята"
                                                    message:@"В ближайшее время с Вами свяжется наш менеджер для подтверждения брони."
                                                   delegate:nil
                                          cancelButtonTitle:@"Спасибо"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) sorryMessagingIsUnavailable {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                    message:@"На вашем устройстве не настроена почта.\nВыберите почтовый профиль в стандартном приложении «Почта»."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)info:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Кальян к Вашему приходу"
                                                    message:@"Вы можете заранее определиться с выбором, и желаемые кальяны уже будут готовы к Вашему приходу."
                                                   delegate:nil
                                          cancelButtonTitle:@"Отлично"
                                          otherButtonTitles:nil];
    [alert show];
}


@end
