//
//  PayViewController.m
//  Hookah Place
//
//  Created by Denis on 22.02.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "PayViewController.h"

@implementation PayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self showinfo:nil];
    
    
    NSString *urlAddress = @"https://money.yandex.ru/to/410011395393986";
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [self.webView loadRequest:requestObj];
    if ([self.webView respondsToSelector:@selector(setKeyboardDisplayRequiresUserAction:)]) {
        self.webView.keyboardDisplayRequiresUserAction = NO;
    }
    [self.webView reload];
}

- (void)viewDidLayoutSubviews {
    self.webView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}


- (IBAction)showinfo:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Дорогие гости, обязательно.."
                                                    message:@"В описании платежа укажите:\nФ.И.О. и номер Вашей клубной карты.\n\nВ противном случае платеж не сможет быть идентифицирован.\n\nСпасибо."
                                                   delegate:nil
                                          cancelButtonTitle:@"Я ознакомлен(а)"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)reload:(id)sender {
     [self.webView reload];
}

- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    CGSize contentSize = theWebView.scrollView.contentSize;
    CGSize viewSize = theWebView.bounds.size;
    
    float rw = viewSize.width / contentSize.width;
    
    theWebView.scrollView.minimumZoomScale = rw;
    theWebView.scrollView.maximumZoomScale = rw;
    theWebView.scrollView.zoomScale = rw;
    
   // function(){document.getElementsByName('uniq1460634181858376616'')[0].focus();},1000
    
    NSString *evalStr = [NSString stringWithFormat:@"setTimeout(function(){document.getElementsByName('uniq1460634181858376616'')[0].focus();},1000"];
    [theWebView stringByEvaluatingJavaScriptFromString:evalStr];
    
    
  
}

@end
