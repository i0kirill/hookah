//
//  PersonalTableViewController.h
//  Hookah Place
//
//  Created by Indieg0 on 14.04.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonalTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bonusQuantityLabel;
@property (strong, nonatomic) NSString *identity;

@end
