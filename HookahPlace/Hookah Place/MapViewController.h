//
//  MapViewController.h
//  Hookah Place
//
//  Created by Denis on 17.01.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
