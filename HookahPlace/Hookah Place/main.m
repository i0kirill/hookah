
//  main.m
//  Hookah Place
//
//  Created by Denis on 17.01.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
