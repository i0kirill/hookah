//
//  RegistrationTableViewController.h
//  Hookah Place
//
//  Created by Indieg0 on 13.04.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *registrateButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

- (IBAction)registrateButtonAction:(id)sender;
- (IBAction)loginButtonAction:(id)sender;




@end
