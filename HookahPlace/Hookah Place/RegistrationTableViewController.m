//
//  RegistrationTableViewController.m
//  Hookah Place
//
//  Created by Indieg0 on 13.04.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "RegistrationTableViewController.h"
#import "AuthorizationTableViewController.h"
#import "PersonalTableViewController.h"
#import "Parse.h"

@interface RegistrationTableViewController ()

@end

@implementation RegistrationTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(handleSingleTap:)];
   
    
    [self.view addGestureRecognizer:tap];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    NSUserDefaults *defautUser = [NSUserDefaults standardUserDefaults];
    
    UIViewController *vc1 = [[self storyboard] instantiateViewControllerWithIdentifier:@"RegistrationTableViewController"]; //if you assigned this ID is storyboard
    UIViewController *vc3 = [[self storyboard] instantiateViewControllerWithIdentifier:@"PersonalTableViewController"];
    
    NSArray *controllers = [NSArray array];
    
    if ([defautUser objectForKey:@"name"] != nil ||
        [defautUser objectForKey:@"lastName"] != nil ||
        [defautUser objectForKey:@"phoneNumber"] != nil) {
        //controllers =  @[vc1, vc3];
        
    
        [self.navigationController pushViewController:vc3 animated:YES];
        
    } else {
        
        //controllers = @[vc1];
    }
    
    NSLog(@"ИМЯ %@", [defautUser objectForKey:@"name"]);
    //  [self.navigationController setViewControllers:controllers];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.nameTextField]) {
        [self.lastNameTextField becomeFirstResponder];
    } else if ([textField isEqual:self.lastNameTextField]) {
        [self.passwordTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender {
    [self.view endEditing:YES];
}

- (IBAction)registrateButtonAction:(id)sender {

   
    if ([self.nameTextField.text isEqualToString:@""] ||
        [self.lastNameTextField.text isEqualToString:@""] ||
        [self.phoneNumberTextField.text isEqualToString:@""] ||
        [self.passwordTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Пожалуйста, заполните все текстовые поля"
                                                       delegate:nil
                                              cancelButtonTitle:@"ОК"
                                              otherButtonTitles:nil];
        [alert show];
    } else {
        
        
        PFObject *user = [PFObject objectWithClassName:@"HookahUser"];
        [user setObject:self.nameTextField.text forKey:@"name"];
        [user setObject:self.lastNameTextField.text forKey:@"lastName"];
        [user setObject:self.phoneNumberTextField.text forKey:@"phoneNumber"];
        [user setObject:self.passwordTextField.text forKey:@"password"];
        [user setObject:@0 forKey:@"bonusQuantity"];
        [user setObject:@0 forKey:@"addBonusQuantity"];
        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if (succeeded){
                
                PFQuery *query = [PFQuery queryWithClassName:@"HookahUser"];
                [query whereKey:@"phoneNumber" equalTo:self.phoneNumberTextField.text];
                [query whereKey:@"password" equalTo:self.passwordTextField.text];
                [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    if (objects.count == 1) {
                        
                        PersonalTableViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"PersonalTableViewController"];
                        vc.identity = [[objects objectAtIndex:0] objectId];
                        //NSLog(@"%@", vc.identity);
                        [self showViewController:vc sender:self];
                        
                        NSUserDefaults *defautUser = [NSUserDefaults standardUserDefaults];
                        [defautUser setObject:[[objects objectAtIndex:0] valueForKey:@"name"] forKey:@"name"];
                        [defautUser setObject:[[objects objectAtIndex:0] valueForKey:@"lastName"] forKey:@"lastName"];
                        [defautUser setObject:[[objects objectAtIndex:0] valueForKey:@"phoneNumber"] forKey:@"phoneNumber"];
                        [defautUser setObject:[[objects objectAtIndex:0] objectId] forKey:@"objectID"];
                        [defautUser setObject:[[[objects objectAtIndex:0] valueForKey:@"bonusQuantity"] stringValue] forKey:@"bonusQuantity"];
                        
                        NSLog(@"Successfully retrieved: %@", objects);
                    } else {
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                                        message:@"Пользователь с таким номером уже существует!"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"ОК"
                                                              otherButtonTitles:nil];
                        [alert show];
                        
                        NSString *errorString = [[error userInfo] objectForKey:@"error"];
                        NSLog(@"Error: %@", errorString);
                    }
                }];
                
            }
            else {
                NSString *errorString = [[error userInfo] objectForKey:@"error"];
                NSLog(@"Error: %@", errorString);
            }
            
        }];

        
    }

    
    
   

    
    
}

- (IBAction)loginButtonAction:(id)sender {

}

@end
