//
//  PersonalTableViewController.m
//  Hookah Place
//
//  Created by Indieg0 on 14.04.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "PersonalTableViewController.h"
#import "QRCodeViewController.h"
#import "Parse.h"

@interface PersonalTableViewController ()

@end

@implementation PersonalTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   // [self.navigationController setNavigationBarHidden:NO];

    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
 
}



-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];    // it shows
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    
    NSUserDefaults *defautUser = [NSUserDefaults standardUserDefaults];
    self.nameLabel.text = [defautUser objectForKey:@"name"];
    self.lastNameLabel.text = [defautUser objectForKey:@"lastName"];



    PFQuery *query = [PFQuery queryWithClassName:@"HookahUser"];
    [query getObjectInBackgroundWithId:[defautUser objectForKey:@"objectID"] block:^(PFObject *hookahUser, NSError *error) {
        if (!error) {
            NSDate *updatedAt = [NSDate date];
            updatedAt = [hookahUser updatedAt];
            
            NSDate *now = [NSDate dateWithTimeIntervalSinceNow:0];
            
            NSTimeInterval interval = [now timeIntervalSinceDate:updatedAt];
            
            NSLog(@"Updated at : %@, Now: %@", updatedAt, now);
            NSLog(@"INTERVAL %f", interval);
            
           
                
                if (interval > 5) { // 86400 = 24 часа
                    int newValue;
                    
                    newValue = [[hookahUser objectForKey:@"bonusQuantity"]intValue] +
                    [[hookahUser objectForKey:@"addBonusQuantity"]intValue];
                    
                    [hookahUser setObject:[NSNumber numberWithInt:newValue] forKey:@"bonusQuantity"];
                    [hookahUser setObject:@0 forKey:@"addBonusQuantity"];
                    [hookahUser saveInBackground];
                    
                    
                    self.bonusQuantityLabel.text = [NSString stringWithFormat:@"%d", newValue];
                } else {
                   self.bonusQuantityLabel.text = [[hookahUser objectForKey:@"bonusQuantity"]stringValue];
                }
                
            
            
            
            NSLog(@"Successfully retrieved");
        } else {
            NSLog(@"Error!");
        }
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath.row == 0 ) {
            return nil;
    } else {
        return indexPath;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 2){
    QRCodeViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"QRCodeViewController"];
        vc.identity = self.identity;
        [self showViewController:vc sender:self];
        
   
    }
}

@end
