//
//  AuthorizationTableViewController.h
//  Hookah Place
//
//  Created by Indieg0 on 13.04.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthorizationTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
- (IBAction)loginButtonAction:(id)sender;

@end
