//
//  AuthorizationTableViewController.m
//  Hookah Place
//
//  Created by Indieg0 on 13.04.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "AuthorizationTableViewController.h"
#import "Parse.h"
#import "PersonalTableViewController.h"

@interface AuthorizationTableViewController ()

@end

@implementation AuthorizationTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(handleSingleTap:)];
    
    [self.view addGestureRecognizer:tap];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.phoneNumberTextField]) {
        [self.passwordTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender {
    [self.view endEditing:YES];
}

- (IBAction)loginButtonAction:(id)sender {
    
    if ([self.phoneNumberTextField.text isEqualToString:@""] ||
        [self.passwordTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Пожалуйста, заполните все текстовые поля"
                                                       delegate:nil
                                              cancelButtonTitle:@"ОК"
                                              otherButtonTitles:nil];
        [alert show];
    } else {
    
    PFQuery *query = [PFQuery queryWithClassName:@"HookahUser"];
    [query whereKey:@"phoneNumber" equalTo:self.phoneNumberTextField.text];
    [query whereKey:@"password" equalTo:self.passwordTextField.text];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (objects.count) {
            
            PersonalTableViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"PersonalTableViewController"];
            vc.identity = [[objects objectAtIndex:0] objectId];
            //NSLog(@"%@", vc.identity);
            [self showViewController:vc sender:self];
            
            NSUserDefaults *defautUser = [NSUserDefaults standardUserDefaults];
            [defautUser setObject:[[objects objectAtIndex:0] valueForKey:@"name"] forKey:@"name"];
            [defautUser setObject:[[objects objectAtIndex:0] valueForKey:@"lastName"] forKey:@"lastName"];
            [defautUser setObject:[[objects objectAtIndex:0] valueForKey:@"phoneNumber"] forKey:@"phoneNumber"];
            [defautUser setObject:[[[objects objectAtIndex:0] valueForKey:@"bonusQuantity"] stringValue] forKey:@"bonusQuantity"];
            [defautUser setObject:[[objects objectAtIndex:0] objectId] forKey:@"objectID"];
            
            NSLog(@"Successfully retrieved: %@", objects);
        } else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                            message:@"Введите корректные номер и пароль"
                                                           delegate:nil
                                                  cancelButtonTitle:@"ОК"
                                                  otherButtonTitles:nil];
            [alert show];
            
            NSString *errorString = [[error userInfo] objectForKey:@"error"];
            NSLog(@"Error: %@", errorString);
        }
    }];
    }
}
@end
