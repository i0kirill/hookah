//
//  SalesViewController.m
//  Hookah Place
//
//  Created by Denis on 17.01.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "SalesViewController.h"

@interface SalesViewController ()

@end

@implementation SalesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            self.kleev.hidden = YES;
        }
        else if(result.height == 568)
        {
            self.kleev.text = @"Разработано\nwww.infinity-apps.ru";
        }
    }
    
    UITapGestureRecognizer* gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(infinityappsClicked)];
    // if labelView is not set userInteractionEnabled, you must do so
    [self.kleev setUserInteractionEnabled:YES];
    [self.kleev addGestureRecognizer:gesture1];
}

- (void) infinityappsClicked {
    NSURL *URL = [NSURL URLWithString:@"http://infinity-apps.ru"];
    [[UIApplication sharedApplication] openURL:URL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)vk:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Переход в группу ВКонтакте" message:nil delegate:self cancelButtonTitle:@"Отмена" otherButtonTitles:@"Перейти", nil];
    alert.tag = 0;
    [alert show];
}
- (IBAction)inst:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Переход на страницу Instagram" message:nil delegate:self cancelButtonTitle:@"Отмена" otherButtonTitles:@"Перейти", nil];
    alert.tag = 1;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ((buttonIndex == 1) && (alertView.tag == 0)) {
        NSURL *URL = [NSURL URLWithString:@"http://vk.com/hookahplacezhuk"];
        [[UIApplication sharedApplication] openURL:URL];
    }
    if ((buttonIndex == 1) && (alertView.tag == 1)) {
        NSURL *URL = [NSURL URLWithString:@"http://instagram.com/hookahplace_zhuk"];
        [[UIApplication sharedApplication] openURL:URL];
    }
}

@end
