//
//  ViewController.m
//  Hookah Place
//
//  Created by Denis on 17.01.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            self.telLabel.hidden = YES;
        }
    }
    
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
    
    [self.view setBackgroundColor: RGB(20, 27, 43)]; //will give a UIColor objct
}

- (IBAction)call:(id)sender {
    NSURL *URL = [NSURL URLWithString:@"tel://89039756824"];
    [[UIApplication sharedApplication] openURL:URL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
