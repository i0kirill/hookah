//
//  OrderTableViewController.h
//  Hookah Place
//
//  Created by Denis on 17.01.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface OrderTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UILabel *label; // number of guests

@property (weak, nonatomic) IBOutlet UITextField *nameLabel; // incorrect name: it's not a label
@property (weak, nonatomic) IBOutlet UITextField *telLabel;
@property (weak, nonatomic) IBOutlet UITextField *timeLabel;
@property (weak, nonatomic) IBOutlet UITextField *dateLabel;
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *shishasTextField;
@end
