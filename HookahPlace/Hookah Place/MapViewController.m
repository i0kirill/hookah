//
//  MapViewController.m
//  Hookah Place
//
//  Created by Denis on 17.01.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "MapViewController.h"
#define METERS_PER_MILE 1609.344

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addPin];
}

- (void) addPin { // Create a Pin on the map
    double lat = 55.761441;
    double lon = 37.584845;
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:CLLocationCoordinate2DMake(lat, lon)];
    [annotation setTitle:@"Bonaparte"];
    [annotation setSubtitle:@"ул. Садовая-Кудринская, д.7 стр.17"];
    [self.mapView addAnnotation:annotation];
}

- (void)viewWillAppear:(BOOL)animated { // Zoom the mapView
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 55.761441;
    zoomLocation.longitude= 37.584845;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.7*METERS_PER_MILE);
    
    // 3
    [self.mapView setRegion:viewRegion animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)goBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
