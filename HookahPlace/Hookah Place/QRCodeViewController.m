//
//  QRCodeViewController.m
//  Hookah Place
//
//  Created by Indieg0 on 14.04.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import "QRCodeViewController.h"


@interface QRCodeViewController ()

@end


@implementation QRCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
  
}

-(void) viewWillAppear:(BOOL)animated {
    NSUserDefaults *defaultUser = [NSUserDefaults standardUserDefaults];
    CIImage *qrCode = [self createQRForString:[defaultUser objectForKey:@"objectID"]];
    
    // Convert to an UIImage
    UIImage *qrCodeImg = [self createNonInterpolatedUIImageFromCIImage:qrCode withScale:2*[[UIScreen mainScreen] scale]];
    
    
    self.mainImageView.image = qrCodeImg;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CIImage *)createQRForString:(NSString *)qrString {
    NSData *stringData = [qrString dataUsingEncoding: NSISOLatin1StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    
    return qrFilter.outputImage;
}

- (UIImage *)createNonInterpolatedUIImageFromCIImage:(CIImage *)image withScale:(CGFloat)scale
{

    CGImageRef cgImage = [[CIContext contextWithOptions:nil] createCGImage:image fromRect:image.extent];
    UIGraphicsBeginImageContext(CGSizeMake(image.extent.size.width * scale, image.extent.size.width * scale));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextDrawImage(context, CGContextGetClipBoundingBox(context), cgImage);
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRelease(cgImage);
    return scaledImage;
}

- (IBAction)doneButtonAction:(id)sender {
}
@end
