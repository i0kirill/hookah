//
//  QRCodeViewController.h
//  Hookah Place
//
//  Created by Indieg0 on 14.04.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface QRCodeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (strong, nonatomic) NSString *identity;

- (IBAction)doneButtonAction:(id)sender;


@end
