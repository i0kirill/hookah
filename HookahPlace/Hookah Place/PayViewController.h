//
//  PayViewController.h
//  Hookah Place
//
//  Created by Denis on 22.02.16.
//  Copyright © 2016 DK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
